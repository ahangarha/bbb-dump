#! /bin/bash

# Copyright 2021 by Mostafa Ahangarha <ahangarha@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

FRAMERATE=30

if [ $# -eq 0 ]; then
    echo "Please give presentation url as an argument"
    exit 1
fi

# Getting base base url

DOMAIN=$(echo $1 | sed -E "s/^(https?:\/\/[^\/]+?).+/\1/")
MEETING_ID=$(echo $1 | sed -E "s/.+meetingId\=(.+)/\1/")

BASE_URL=$DOMAIN/presentation/$MEETING_ID

# Getting critical files
wget -nc $BASE_URL/shapes.svg
wget -nc $BASE_URL/video/webcams.webm
wget -nc $BASE_URL/deskshare/deskshare.webm
wget -nc $BASE_URL/deskshare.xml

# Get Slides

# Get timestamp, path, filename
sed -E 's/.+in="([0-9\.]+)".+xlink:href="(.+-)(.+)\/(slide-[0-9]+.png).+/\1\t\2\3\/\4\t\3-\4/' shapes.svg | grep "^[0-9]" > slides-timestamp.csv

mkdir -p slides

# Ensure marker file is empty and not contain anything from previous failed run
echo "" > markers.txt

echo "Downloading slides and generating markers. Please wait..."

while read line; do
    time=$(echo $line | cut -f1 -d ' ' | cut -f1 -d '.')
    path=$(echo $line | cut -f2 -d ' ')
    name=$(echo $line | cut -f3 -d ' ')
    
    pos=$(($time * 30))
    
    slide_url=$BASE_URL/$path
    
    echo -n "Downloading: slides/"$name

    wget -nc -q $slide_url -O "slides/"$name
    
    echo " - DONE"
    
    echo -n "Making marker..."
    echo "        {
        \"comment\": \"$name\",
        \"pos\": $(($time*$FRAMERATE)),
        \"type\": 0
    }," >> markers.txt
    
    echo " - DONE"

done < slides-timestamp.csv

echo "Tasks compeleted."
