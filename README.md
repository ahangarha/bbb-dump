# BigBlueButton Dump

A simple bash script to download media files from a recorded session on BigBlueButton.

## Usage

Copy the `bbb-dump` file in a directory which is mentioned in `PATH` variable and make it executable. Then go to the desired project directory and simple run `bbb-dump <path-to-public-presentation>`.

## License
**BigBlueButton Dump** is licensed under GPL-v3.
